package se.pocketmobile.materialdemo;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toolbar;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ManifestsFragment extends Fragment implements ViewPager.OnPageChangeListener {
    @InjectView(R.id.pager) ViewPager pager;
    @InjectView(R.id.actionbar) Toolbar actionbar;
    @InjectView(R.id.tabs) IndicatorLinearLayout tabs;
    @InjectView(R.id.tab1) Button tab1;
    @InjectView(R.id.tab2) Button tab2;
    @InjectView(R.id.tab3) Button tab3;

    private DrawerOpener navigationDrawerOpener;

    public static ManifestsFragment newInstance() {
        ManifestsFragment manifestsFragment = new ManifestsFragment();
        Bundle args = new Bundle();
        args.putString("title", "Manifest");
        manifestsFragment.setArguments(args);
        return manifestsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        navigationDrawerOpener = (DrawerOpener) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manifests, container, false);
        ButterKnife.inject(this, view);
        actionbar.setNavigationIcon(R.drawable.ic_action_navigation_menu);
        actionbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationDrawerOpener.openDrawer();
            }
        });
        tab1.setOnClickListener(createTabClickListener(0));
        tab2.setOnClickListener(createTabClickListener(1));
        tab3.setOnClickListener(createTabClickListener(2));
        return view;
    }

    private View.OnClickListener createTabClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(position, true);
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pager.setOnPageChangeListener(this);
        pager.setAdapter(new ManifestsPagerAdapter(getChildFragmentManager()));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        tabs.setOffset(position, positionOffset);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class ManifestsPagerAdapter extends FragmentPagerAdapter {
        public ManifestsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new ManifestListFragment();
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
