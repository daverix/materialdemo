package se.pocketmobile.materialdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toolbar;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ManifestActivity extends Activity {
    @InjectView(R.id.actionbar) Toolbar actionbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_manifest);
        ButterKnife.inject(this);

        actionbar.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back);
        actionbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAfterTransition();
            }
        });
    }
}
