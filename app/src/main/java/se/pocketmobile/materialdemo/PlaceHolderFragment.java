package se.pocketmobile.materialdemo;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toolbar;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PlaceHolderFragment extends Fragment {
    private static final String ARG_TITLE = "title";
    private String title;
    private DrawerOpener navigationDrawerOpener;

    @InjectView(R.id.text) TextView textView;
    @InjectView(R.id.actionbar) Toolbar actionbar;

    public static PlaceHolderFragment newInstance(String title) {
        PlaceHolderFragment fragment = new PlaceHolderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        navigationDrawerOpener = (DrawerOpener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        title = getArguments().getString(ARG_TITLE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_placeholder, container, false);
        ButterKnife.inject(this, view);
        textView.setText(title);
        actionbar.setTitle(title);
        actionbar.setNavigationIcon(R.drawable.ic_action_navigation_menu);
        actionbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationDrawerOpener.openDrawer();
            }
        });
        return view;
    }
}
