package se.pocketmobile.materialdemo;

public interface DrawerOpener {
    void openDrawer();
}
