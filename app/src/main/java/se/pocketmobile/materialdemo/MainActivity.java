package se.pocketmobile.materialdemo;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener, DrawerOpener {
    @InjectView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @InjectView(R.id.left_drawer) ListView drawer;

    private String[] items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        drawer.addHeaderView(getLayoutInflater().inflate(R.layout.profile_header, drawer, false));
        items = getResources().getStringArray(R.array.drawer_items);
        drawer.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items));
        drawer.setOnItemClickListener(this);

        Fragment fragment = getFragmentManager().findFragmentById(R.id.content_frame);
        if(fragment == null) {
            fragment = ManifestsFragment.newInstance();
            getFragmentManager().beginTransaction().add(R.id.content_frame, fragment).commit();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0) return;

        String title = items[position-1];
        if(position == 1) {
            getFragmentManager().beginTransaction().replace(R.id.content_frame, ManifestsFragment.newInstance()).commit();
        }
        else {
            getFragmentManager().beginTransaction().replace(R.id.content_frame, PlaceHolderFragment.newInstance(title)).commit();
        }

        drawerLayout.closeDrawer(drawer);
    }

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(drawer);
    }
}
