package se.pocketmobile.materialdemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.PathInterpolator;
import android.widget.LinearLayout;

public class IndicatorLinearLayout extends LinearLayout {
    private static final int SELECTED_INDICATOR_THICKNESS_DIPS = 2;

    private final int mSelectedIndicatorThickness;
    private final Paint mSelectedIndicatorPaint;

    private int mSelectedPosition;
    private float mOffset;
    private PathInterpolator pathInterpolator;

    public IndicatorLinearLayout(Context context) {
        this(context, null);
    }

    public IndicatorLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);

        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.colorAccent, outValue, true);
        final int accentColor =  outValue.data;

        final float density = getResources().getDisplayMetrics().density;
        mSelectedIndicatorThickness = (int) (SELECTED_INDICATOR_THICKNESS_DIPS * density);
        mSelectedIndicatorPaint = new Paint();
        mSelectedIndicatorPaint.setColor(accentColor);

        if(!isInEditMode()) {
            pathInterpolator = new PathInterpolator(0, 0.5f, 0.75f, 1);
        }
    }

    public void setOffset(int position, float offset) {
        mSelectedPosition = position;
        mOffset = offset;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final int height = getHeight();
        final int childCount = getChildCount();

        if (childCount > 0) {
            View selectedTitle = getChildAt(mSelectedPosition);
            int left;
            int right;

            if (mOffset != 0 && pathInterpolator != null) {
                View next = getNext(mSelectedPosition, childCount);
                float leftInterpolated = 1-pathInterpolator.getInterpolation(1-mOffset);
                float rightInterpolated = pathInterpolator.getInterpolation(mOffset);
                left = (int) (selectedTitle.getLeft() + leftInterpolated * selectedTitle.getWidth());
                right = (int) (selectedTitle.getRight() + rightInterpolated * next.getWidth());
            } else {
                left = selectedTitle.getLeft();
                right = selectedTitle.getRight();
            }

            canvas.drawRect(left, height - mSelectedIndicatorThickness, right,
                    height, mSelectedIndicatorPaint);
        }
    }

    private View getNext(int index, int childCount) {
        if(index >= childCount - 1)
            return getChildAt(index);

        return getChildAt(index + 1);
    }
}
