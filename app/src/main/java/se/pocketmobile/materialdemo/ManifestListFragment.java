package se.pocketmobile.materialdemo;

import android.app.ActivityOptions;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ManifestListFragment extends Fragment {
    @InjectView(R.id.manifestCards) RecyclerView manifestCards;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manifest_list, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        manifestCards.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        manifestCards.setAdapter(new ManifestCardsAdapter(getActivity().getLayoutInflater()));
    }

    private class ManifestCardsAdapter extends RecyclerView.Adapter<ManifestHolder> {
        private final LayoutInflater inflater;

        public ManifestCardsAdapter(LayoutInflater inflater) {
            this.inflater = inflater;
        }

        @Override
        public ManifestHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return ManifestHolder.newInstance(inflater.inflate(R.layout.manifest_card_item, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(ManifestHolder manifestHolder, int i) {
            manifestHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                            getActivity(), v, "manifestCard");
                    startActivity(new Intent(getActivity(), ManifestActivity.class), options.toBundle());
                }
            });
        }

        @Override
        public int getItemCount() {
            return 20;
        }
    }

    private static class ManifestHolder extends RecyclerView.ViewHolder {
        public static ManifestHolder newInstance(View itemView) {
            return new ManifestHolder(itemView);
        }

        private ManifestHolder(View itemView) {
            super(itemView);

        }
    }
}
